package comp2912.app;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request.Method;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.apache.http.params.HttpParams;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import comp2912.app.AppConfig;
import comp2912.app.SQLHandler;
import comp2912.app.AppController;

public class DataLogger extends AppCompatActivity {
    private Button submit;
    private EditText DLText;
    private SQLHandler db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_data_logger);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        submit = findViewById(R.id.button_submit);
        DLText = findViewById(R.id.data_logger_text);
        db = new SQLHandler(getApplicationContext());

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!DLText.getText().toString().isEmpty()){
                    sendActivity(DLText.getText().toString());
                    DLText.getText().clear();
                }
            }
        });
    }

    private void sendActivity(final String activity) {
        StringRequest strReq = new StringRequest(Method.POST, AppConfig.URL_CONNECT, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");
                    if (!error) { //Success!
                        JSONObject newActivity = jObj.getJSONObject("newActivity");
                        String activity = newActivity.getString("activity");
                        db.addActivity(activity);
                        Toast.makeText(getApplicationContext(), "Activity successfully submitted", Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error){
                Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_LONG).show();
                Toast.makeText(getApplicationContext(), "Something went wrong", Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("activity", activity);
                return params;
            }
        };

        AppController.getInstance().addToRequestQueue(strReq, "req_register");
    }
}