package comp2912.app;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.HashMap;

public class SQLHandler extends SQLiteOpenHelper {
    private static final String TAG = SQLHandler.class.getSimpleName();
    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "app";
    private static final String TABLE_ACTIVITIES = "activities";
    private static final String KEY_ID = "id";
    private static final String KEY_ACTIVITY = "activity";
    private static final String KEY_SENT_AT = "sent_at";

    public SQLHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) { //Creates the table
        String CREATE_TABLE = "CREATE TABLE " + TABLE_ACTIVITIES + "(" + KEY_ID + "INTEGER PRIMARY KEY, " + KEY_ACTIVITY + "TEXT, " + KEY_SENT_AT + "DATETIME)";
        db.execSQL(CREATE_TABLE);

        Log.d(TAG, "Database tables created");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) { //Updates the table
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_ACTIVITIES);
        onCreate(db);
    }

    public void addActivity(String activity) { //Adds the activity the user enters to the table
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_ACTIVITY, activity);

        long id = db.insert(TABLE_ACTIVITIES, null, values);
        db.close();

        Log.d(TAG, "New activity inserted into table: " + id);
    }

    public HashMap<String, String> getActivities() {
        HashMap<String, String> activity = new HashMap<String, String>();
        String selectQuery = "SELECT * FROM " + TABLE_ACTIVITIES;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        cursor.moveToFirst();
        if(cursor.getCount() > 0) {
            activity.put("id", cursor.getString(1));
            activity.put("activity", cursor.getString(2));
            activity.put("sent_at", cursor.getString(3));
        }
        cursor.close();
        db.close();
        Log.d(TAG, "Fetching activity from table: " + activity.toString());

        return activity;
    }
}
