<?php
include 'config.php';

class Connect {
	private $connect;

	public function __construct() {
		$this->connect = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);

		if (mysqli_connect_errno($this->connect)) {
			echo "Could not connect to MySQL";
		}
	}

	public function getDatabase() {
		return $this->connect;
	}
}
?>